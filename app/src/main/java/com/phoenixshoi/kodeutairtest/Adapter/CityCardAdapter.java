package com.phoenixshoi.kodeutairtest.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phoenixshoi.kodeutairtest.Object.City;
import com.phoenixshoi.kodeutairtest.R;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 08.08.17.
 */

public class CityCardAdapter extends RecyclerView.Adapter<CityCardAdapter.ViewHolder> {
    private ArrayList<City> mCityData;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView cityInfo;
        public TextView airportInfo;

        public ViewHolder(View v) {
            super(v);
            cityInfo = v.findViewById(R.id.city_text_info);
            airportInfo = v.findViewById(R.id.airport_text_info);
        }
    }

    public CityCardAdapter(ArrayList<City> mCityData) {
        this.mCityData = mCityData;
    }

    @Override
    public CityCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.city_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cityInfo.setText(mCityData.get(position).getNameCity());
        holder.airportInfo.setText(mCityData.get(position).getAirportCity());

    }

    @Override
    public int getItemCount() {
        return mCityData.size();
    }
}