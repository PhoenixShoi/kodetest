package com.phoenixshoi.kodeutairtest.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.phoenixshoi.kodeutairtest.R;

public class PurchaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);

        Intent intent = new Intent(this, SelectCityActivity.class);
        startActivity(intent);

    }

}
