package com.phoenixshoi.kodeutairtest.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.phoenixshoi.kodeutairtest.Adapter.CityCardAdapter;
import com.phoenixshoi.kodeutairtest.Object.City;
import com.phoenixshoi.kodeutairtest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SelectCityActivity extends AppCompatActivity {

    private RecyclerView mCityRecyclerView;
    private RecyclerView.Adapter mCityRecyclerAdapter;
    private RecyclerView.LayoutManager mCityRecyclerLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_city);
        AndroidNetworking.initialize(getApplicationContext());

        ini();
        //getCityData();
        updateCityRecyclerView();
    }

    private void ini(){
        mCityRecyclerView = (RecyclerView) findViewById(R.id.city_recycler_view);
        mCityRecyclerView.setHasFixedSize(true);
        mCityRecyclerLayoutManager = new LinearLayoutManager(this);
        mCityRecyclerView.setLayoutManager(mCityRecyclerLayoutManager);
    }

    private void updateCityRecyclerView(){

        AndroidNetworking.get("https://api.meetup.com/cities?country=ru&key=a264921d267593116d2f299481a")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener(){

                    @Override
                    public void onResponse(JSONObject response) {
                        showToast("NetworkComplete ");
                        try {
                            ArrayList<City> mCityData = new ArrayList<>();
                            JSONArray cityNetworkData = response.getJSONArray("results");
                            for(int i=0;i<cityNetworkData.length();i++){
                                mCityData.add(new City(cityNetworkData.getJSONObject(i).get("city").toString(),"Все аэропорты"));
                            }
                            mCityRecyclerAdapter = new CityCardAdapter(mCityData);
                            mCityRecyclerView.setAdapter(mCityRecyclerAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        showToast("NetworkError "+error.toString());
                    }
                });
    }

    private void showToast(String message){
        Toast toast = Toast.makeText(getApplicationContext(),
                message, Toast.LENGTH_LONG);
        toast.show();
    }

}
