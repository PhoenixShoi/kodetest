package com.phoenixshoi.kodeutairtest.Object;

/**
 * Created by phoenixshoi on 19.08.17.
 */

public class City{
    private String mNameCity;
    private String mAirportCity;

    public City(String mNameCity, String mAirportCity){
        this.mNameCity = mNameCity;
        this.mAirportCity = mAirportCity;
    }


    public String getNameCity() {
        return mNameCity;
    }

    public void setNameCity(String mNameCity) {
        this.mNameCity = mNameCity;
    }

    public String getAirportCity() {
        return mAirportCity;
    }

    public void setAirportCity(String mAirportCity) {
        this.mAirportCity = mAirportCity;
    }
}
